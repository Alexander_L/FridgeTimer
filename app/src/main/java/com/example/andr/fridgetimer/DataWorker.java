package com.example.andr.fridgetimer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import android.preference.PreferenceManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

class DataWorker extends Activity{

    public static DataWorker instance;
    public static final String APP_DATA = "holdProductsData";
    public static final String DELIMETER_KEY = "---===---";
    ArrayList holdProducts = MainActivity.getInstance().holdProducts;
    private static final String DATA_FILE_NAME = "products.txt";

    public static DataWorker getInstance() {
        if (instance == null) {
            instance = new DataWorker();}
        return instance;
    }

    protected void saveDataToPrefs (Context ctx) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        Product productToSave;
        String strToSave = null;
        String [] listOfProducts = new String[holdProducts.size()];
        Editor ed = sPref.edit();
        Set <String> dataToSave = new HashSet<String>();
        for (int i = 0; i < holdProducts.size(); i++) {
            productToSave = (Product) holdProducts.get(i);
            strToSave = productToSave.getFoodToHold().concat(DELIMETER_KEY).concat(Integer.toString(productToSave.getKeepDays()));
            listOfProducts[i] = strToSave; //mb useless
            dataToSave.add(strToSave);
        }
        ed.putStringSet(APP_DATA,dataToSave);
        ed.apply();
        MainActivity.getInstance().showSaveDataMessage();
    }

    protected void loadDataFromPrefs(Context ctx) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        Product productToLoad;
        if (sPref.contains(APP_DATA)) {
            Set<String> loadedStringsFromPrefs = sPref.getStringSet(APP_DATA, new HashSet<String>());
            String [] unsortedStrings = new String[loadedStringsFromPrefs.size()];
            unsortedStrings = (String[]) loadedStringsFromPrefs.toArray(unsortedStrings);
            for (int i = 0; i < unsortedStrings.length; i++){
                String [] textProduct = unsortedStrings[i].split(DELIMETER_KEY);
                String productName = textProduct[0];
                int keepDays = Integer.parseInt(textProduct[1]);
                productToLoad = new Product(productName, keepDays); //or use constructor in another class?
                holdProducts.add(productToLoad); //Maybe it must create at Product class?
            }
            MainActivity.getInstance().showLoadDataMessage();
            MainActivity.getInstance().listAdapter.notifyDataSetChanged();
        }
        else {
            MainActivity.getInstance().showEmptyMessage();
        }
    }

    protected void saveDataToFile () {
        if(holdProducts.size() > 0 ) {
            String strToSave = null;
            Product productToSave;

            try {
                File myFile = new File(Environment.getExternalStorageDirectory().toString() + "/" + DATA_FILE_NAME);
                myFile.createNewFile();
                FileOutputStream outputStream = new FileOutputStream(myFile);
                for (int i = 0; i < holdProducts.size(); i++) {
                    productToSave = (Product) holdProducts.get(i);
                    strToSave = productToSave.getFoodToHold().concat(DELIMETER_KEY).concat(Integer.toString(productToSave.getKeepDays())) + "\n";
                    outputStream.write(strToSave.getBytes());
                }
                MainActivity.getInstance().showSaveDataMessage();
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else {
            MainActivity.getInstance().showEmptyMessage();
        }
    }

    protected void loadDataFromFile() {
        Product productToLoad;
        String line = null;
        File myFile = new File(Environment.getExternalStorageDirectory().toString() + "/" + DATA_FILE_NAME);
        try {
            FileInputStream inputStream = new FileInputStream(myFile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            try {
                while ((line = bufferedReader.readLine()) != null) {
                    line.substring(0, line.length() - 1);
                    String[] textProduct = line.split(DELIMETER_KEY);
                    String productName = textProduct[0];
                    int keepDays = Integer.parseInt(textProduct[1]);
                    productToLoad = new Product(productName, keepDays);
                    holdProducts.add(productToLoad);
                }
            } catch (IOException e) {
                e.printStackTrace();
                MainActivity.getInstance().showErrorMessage();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            MainActivity.getInstance().showErrorMessage();
        }
        MainActivity.getInstance().showLoadDataMessage();
        MainActivity.getInstance().listAdapter.notifyDataSetChanged();
    }

}
