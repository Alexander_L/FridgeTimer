package com.example.andr.fridgetimer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GestureDetectorCompat;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected static MainActivity instance;
    protected ListView lvHoldProductsList;
    public ArrayList<Product> holdProducts;
    ListAdapter listAdapter;
    private GestureDetectorCompat lSwipeDetector;
    Context ctx = this;
    String foodName;
    int keepDays;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        instance = this;

        lvHoldProductsList = findViewById(R.id.productsHoldListView);
        holdProducts = ProductsInFridge.getInstance().getHoldProducts();
        listAdapter = new ListAdapter(this, holdProducts);
        lvHoldProductsList.setAdapter(listAdapter);

        lSwipeDetector = new GestureDetectorCompat(this, new MyGestureListener());
        lvHoldProductsList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return lSwipeDetector.onTouchEvent(event);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFoodNameAlertInput();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {


        } else if (id == R.id.nav_slideshow) {
            DataWorker.getInstance().saveDataToPrefs(this);

        } else if (id == R.id.nav_manage) {
            DataWorker.getInstance().loadDataFromPrefs(this);

        } else if (id == R.id.nav_share) {
            DataWorker.getInstance().saveDataToFile();


        } else if (id == R.id.nav_send) {
            DataWorker.getInstance().loadDataFromFile();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    protected Product setProductInFridge() {
        return new Product(foodName, keepDays);
    }

    public static MainActivity getInstance() {
        return instance;
    }

    private void getFoodNameInputIntent() {
        Intent intent = new Intent(this, FoodNameInput.class);
        startActivity(intent);
    }

    protected void showTestMessage(int positionFood) {
        Toast.makeText(this, R.string.removed_position + " " + positionFood, Toast.LENGTH_SHORT).show();
    }

    protected void showEmptyMessage () {
        Toast.makeText(this, R.string.fridge_is_empty, Toast.LENGTH_SHORT).show();
    }

    protected void showErrorMessage() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
    }

    protected void showSaveDataMessage() {
        Toast.makeText(this, R.string.save_complete, Toast.LENGTH_SHORT).show();
    }

    protected void showLoadDataMessage () {
        Toast.makeText(this, R.string.load_complete, Toast.LENGTH_SHORT).show();
    }

    protected void setFoodNameAlertInput() { //Too long method name.

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.foodNameAlertTitle);
        alert.setMessage(R.string.foodNameAlertHint);

        final AutoCompleteTextView input = new AutoCompleteTextView(this);
        input.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, FoodStorage.getInstance().getFoodStrings()));
        input.setSingleLine(true);
        alert.setView(input);

        alert.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (input.getText().length() <= 0) {
                    Toast.makeText(getApplicationContext(), R.string.foodNameNullToast, Toast.LENGTH_LONG).show();
                }
                else {
                    setFoodName((input.getText()).toString());
                    dialog.dismiss();
                    callDataPicker();
                }
            }
        });

        alert.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        alert.show();

    }


    protected void setFoodName( String foodName) {
        this.foodName = foodName;
    }

    protected void setKeepDays (int keepDays) {
        this.keepDays = keepDays;
    }

    protected String getFoodName () {
        return foodName;
    }

    protected int getKeepDays () {
        return keepDays;
    }

    private void callDataPicker() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

}
