package com.example.andr.fridgetimer;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

class DateWorker {
    protected static DateWorker instance;
    private int keepingDays;

    protected Calendar getCurrentCalendar() {
        return  Calendar.getInstance();
    }

    protected static DateWorker getInstance() {
        if (instance == null) {
            instance = new DateWorker();
        }
        return instance;
    }

    protected void setKeepingDays (int year, int month, int day) {
        long currentTime = getCurrentCalendar().getTimeInMillis();
        Calendar inputCalendar = getCurrentCalendar();
        inputCalendar.set(year, month, day);
        long timeInMillsToDays = (inputCalendar.getTimeInMillis()) - currentTime;
        keepingDays = (int)(timeInMillsToDays / (24 * 60 * 60 * 1000));
        MainActivity.getInstance().setKeepDays(keepingDays);


        /*if (keepingMonths > -1 || keepingYears > 0 ) {     вот здесь перегонять все в дни! Месяца возвращаются с 0.

        }
        */


    }

    protected int getKeepingDays () {
        return keepingDays;
    }
/*
    protected int testKeepDays (){
        return 3;
    }*/




}
