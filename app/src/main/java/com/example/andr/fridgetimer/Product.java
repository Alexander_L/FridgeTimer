package com.example.andr.fridgetimer;

class Product {
    //protected FoodStorage.Food product = FoodStorage.Food.MILK; //FoodStorage.getInstance().testGetFood();
    private String foodToHold;
    private int keepDays;//DateWorker.getInstance().testKeepDays(); Валится с ошибкой вывода списка, если не String


    Product (String foodToHold, int keepDays){
        this.foodToHold = foodToHold;
        this.keepDays = keepDays;
    }

    protected String getFoodToHold() {
        return foodToHold.substring(0,1).toUpperCase() + foodToHold.substring(1).toLowerCase();
    }

    protected int getKeepDays() {
        return keepDays;
    }

   /* protected int getKeepDays () { //перенести преобразование в адаптер
        setKeepDays();
        return keepDays;
    }

    protected FoodStorage.Food getFoodToHold() {
        setFoodToHold();
        return foodToHold;
    }

    private void setKeepDays () {
        keepDays = DateWorker.getInstance().testKeepDays();
    }

    private void setFoodToHold () {
        foodToHold = FoodStorage.getInstance().testGetFood();
    }
*/

}
