package com.example.andr.fridgetimer;

import java.util.ArrayList;

class ProductsInFridge {
    protected static ProductsInFridge instance;
    private ArrayList<Product> holdProducts = new ArrayList<>();

    /*protected Product setProductToFridge () {
        Product product = new Product();
        return product;
    }*/

    protected static ProductsInFridge getInstance () {
        if (instance == null) {
            instance = new ProductsInFridge();
        }
        return instance;
    }

    protected ArrayList<Product> getHoldProducts () {
        return holdProducts;
    }

}
