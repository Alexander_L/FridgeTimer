package com.example.andr.fridgetimer;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;

class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

    private static final int SWIPE_MIN_DISTANCE = 130;
    private static final int SWIPE_MAX_DISTANCE = 300;
    private static final int SWIPE_MIN_VELOCITY = 200;

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
        if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_DISTANCE)
            return false;
        if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_MIN_VELOCITY) {
            MainActivity.getInstance().lvHoldProductsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    MainActivity.getInstance().showTestMessage(i);
                    MainActivity.getInstance().holdProducts.remove(i);
                    MainActivity.getInstance().listAdapter.notifyDataSetChanged();
                }
            });


        }
        return false;
    }
}